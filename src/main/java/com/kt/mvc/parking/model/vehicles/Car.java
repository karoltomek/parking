package com.kt.mvc.parking.model.vehicles;

import com.kt.mvc.parking.model.Price;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Car extends Vehicle {

    private int vehicleId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    @Override
    public Price getRate() {
        return Price.CAR;
    }

    @Override
    public String toString() {
        return "Auto, ID: " + vehicleId;
    }


}
