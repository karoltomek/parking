package com.kt.mvc.parking.model.parkingSpace;

import java.util.List;

public interface ParkingSpaceInterface {

    void addParkingSpace();

    List<Integer> getParkedVehicleIdList();

    List<ParkingSpace> getFreeParkingSpaces();

    List<ParkingSpace> getParkingSpacesList();

    int getParkingCapacity();

    void increaseParkingCapacity(int newCapacity);

}
