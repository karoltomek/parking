package com.kt.mvc.parking.model.vehicles;

import com.kt.mvc.parking.model.Price;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@Getter
@Setter
public class HistoryVehicle extends Vehicle {

    private int vehicleId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Price rate;

    public HistoryVehicle(Vehicle vehicle) {
        this.vehicleId = vehicle.getVehicleId();
        this.startTime = vehicle.getStartTime();
        this.endTime = vehicle.getEndTime();
        this.rate = vehicle.getRate();
    }
}
