package com.kt.mvc.parking.model.vehicles;

import java.util.List;

public interface VehicleInterface {

    void addVehicle(Vehicle vehicle);

    Car createCar();

    Truck createTruck();

    Motorcycle createMotorcycle();

    void removeVehicle(int vehicleId);

    List<Vehicle> getAuthorizedVehicleList();

}
