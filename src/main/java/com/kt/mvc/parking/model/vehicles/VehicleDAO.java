package com.kt.mvc.parking.model.vehicles;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VehicleDAO implements VehicleInterface {

    private List<Vehicle> authorizedVehicleList = new ArrayList<>();

    public void addVehicle(Vehicle vehicle) {

        if (authorizedVehicleList.isEmpty()) {
            vehicle.setVehicleId(1);
        } else {
            vehicle.setVehicleId(authorizedVehicleList.get(authorizedVehicleList.size() - 1).getVehicleId() + 1);
        }
        authorizedVehicleList.add(vehicle);

    }

    public Car createCar() {
        return new Car();
    }

    public Truck createTruck() {
        return new Truck();
    }

    public Motorcycle createMotorcycle() {
        return new Motorcycle();
    }

    public void removeVehicle(int vehicleId) {
        if (authorizedVehicleList.stream().anyMatch(p -> p.getVehicleId() == vehicleId)) {
            authorizedVehicleList = authorizedVehicleList.stream()
                    .filter(p -> p.getVehicleId() != vehicleId)
                    .collect(Collectors.toList());
        }
    }

    public List<Vehicle> getAuthorizedVehicleList() {
        return authorizedVehicleList;
    }
    public void reset(){
        authorizedVehicleList = new ArrayList<>();
    }
}
