package com.kt.mvc.parking.controller.parkingController;

import com.kt.mvc.parking.controller.printer.PrinterMVC;
import com.kt.mvc.parking.controller.scanner.ScannerMVC;

public class ParkingController implements ParkingMVC.Controller {

    private ParkingMVC.Model parking;
    private ScannerMVC.Controller scannerController;
    private PrinterMVC.Controller printerController;

    public ParkingController(ParkingMVC.Model parking, ScannerMVC.Controller scannerController, PrinterMVC.Controller printerController) {
        this.scannerController = scannerController;
        this.printerController = printerController;
        this.parking = parking;
    }


    public void manageParking() {

        printerController.println("Wybierz opcję");
        printerController.println("1. Dodaj auto do rejestru");
        printerController.println("2. Dodaj motocykl do rejestru");
        printerController.println("3. Dodaj ciężarówkę do rejestru");
        printerController.println("4. Usuń pojazd z rejestru");
        printerController.println("5. Wjedź na parking");
        printerController.println("6. Wyjedź z parkingu");
        printerController.println("7. Pokaż zaparkowane pojazdy");
        printerController.println("8. Pokaż wolne miejsca");
        printerController.println("9. Dzisiejsze opłaty");
        printerController.println("10. Historia");
        printerController.println("11. Zmień liczbę miejsc parkingowych");
        printerController.println("12. Wyłącz program");

        int choice = scannerController.pickOption();

        switch (choice) {
            case 1:
                parking.getVehicleDAO().addVehicle(parking.getVehicleDAO().createCar());
                manageParking();
                break;
            case 2:
                parking.getVehicleDAO().addVehicle(parking.getVehicleDAO().createMotorcycle());
                manageParking();
                break;
            case 3:
                parking.getVehicleDAO().addVehicle(parking.getVehicleDAO().createTruck());
                manageParking();
                break;
            case 4:
                printerController.println("Pojazdy w bazie");
                printerController.println(parking.getVehicleDAO().getAuthorizedVehicleList());
                printerController.println("Podaj ID pojazdu");
                parking.getVehicleDAO().removeVehicle(scannerController.nextInt());
                manageParking();
                break;
            case 5:
                printerController.println("Pojazdy w bazie");
                printerController.println(parking.getVehicleDAO().getAuthorizedVehicleList());
                printerController.println("Podaj ID pojazdu");
                parking.parkVehicle(scannerController.nextInt());
                manageParking();
                break;
            case 6:
                printerController.println("Zaparkowane pojazdy");
                printerController.println(parking.getParkingSpaceDAO().getParkedVehicleIdList());
                printerController.println("Podaj ID pojazdu");
                parking.unparkVehicle(scannerController.nextInt());
                manageParking();
                break;
            case 7:
                parking.showParkedVehicles();
                manageParking();
                break;
            case 8:
                printerController.println("Wolne miejsca");
                printerController.println(parking.getParkingSpaceDAO().getFreeParkingSpaces());
                manageParking();
                break;
            case 9:
                printerController.println("Dzisiejsze opłaty");
                printerController.println(parking.calculatePriceForDay());
                manageParking();
                break;
            case 10:
                printerController.println("Historia parkowań");
                printerController.println(parking.getHistoryVehicleDAO().getVehicleHistoryList());
                manageParking();
                break;
            case 11:
                printerController.println("Podaj nową liczbę miejsc");
                parking.getParkingSpaceDAO().increaseParkingCapacity(scannerController.nextInt());
                manageParking();
                break;
            case 12:

                break;
            default:
                manageParking();
                break;
        }

    }
}
