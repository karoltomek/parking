package com.kt.mvc.parking.controller.printer;

public interface PrinterMVC {
    interface Controller {

        void println(Object object);

    }
}
