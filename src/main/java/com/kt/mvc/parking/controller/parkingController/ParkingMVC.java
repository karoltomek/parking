package com.kt.mvc.parking.controller.parkingController;

import com.kt.mvc.parking.model.parkingSpace.ParkingSpaceDAO;
import com.kt.mvc.parking.model.vehicles.HistoryVehicleDAO;
import com.kt.mvc.parking.model.vehicles.VehicleDAO;

public interface ParkingMVC {
    interface Model{

        double calculatePrice(int vehicleId);

        double calculatePriceForDay();

        void parkVehicle(int vehicleId);

        void unparkVehicle(int vehicleId);

        ParkingSpaceDAO getParkingSpaceDAO();

        VehicleDAO getVehicleDAO();

        void showParkedVehicles();

        HistoryVehicleDAO getHistoryVehicleDAO();

        boolean checkFreeSpaces();

        boolean permissionForPark(int vehicleId);

        void reset();
    }

    interface Controller{

        void manageParking();

    }

    interface View{

        void start();

    }
}
