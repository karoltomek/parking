package com.kt.mvc.parking;

import com.kt.mvc.parking.controller.parkingController.ParkingMVC;
import com.kt.mvc.parking.view.ParkingView;

public class Runner {

    public static void main(String[] args) {
        ParkingMVC.View parkingView = new ParkingView();
        parkingView.start();
    }

}

