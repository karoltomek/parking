package com.kt.mvc.parking.model.vehicles;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VehicleDAOTest {

    Car car;
    Truck truck;
    Motorcycle motorcycle;
    VehicleDAO vehicleDAO;

    @BeforeEach
    void setup() {
        car = new Car();
        truck = new Truck();
        motorcycle = new Motorcycle();
        vehicleDAO = new VehicleDAO();

        addVehicles();
    }

    void addVehicles() {
        vehicleDAO.addVehicle(car);
        vehicleDAO.addVehicle(truck);
        vehicleDAO.addVehicle(motorcycle);
    }

    @Test
    void shouldContainThreeVehiclesWhenThreeAdded() {
        // when then
        assertEquals(vehicleDAO.getAuthorizedVehicleList().size(), 3);
    }

    @Test
    void shouldContainTwoVehiclesWhenOneReomved() {
        //when
        vehicleDAO.removeVehicle(3);
        //then
        assertEquals(vehicleDAO.getAuthorizedVehicleList().size(), 2);
    }

    @Test
    void shouldContainThreeDifferentVehiclesWhenThreeAdded() {
        //when then
        assertAll(
                () -> assertTrue(vehicleDAO.getAuthorizedVehicleList().contains(car)),
                () -> assertTrue(vehicleDAO.getAuthorizedVehicleList().contains(truck)),
                () -> assertTrue(vehicleDAO.getAuthorizedVehicleList().contains(motorcycle))
        );
    }
}