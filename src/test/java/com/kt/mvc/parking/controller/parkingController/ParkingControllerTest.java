package com.kt.mvc.parking.controller.parkingController;

import com.kt.mvc.parking.controller.printer.PrinterMVC;
import com.kt.mvc.parking.controller.scanner.ScannerMVC;
import com.kt.mvc.parking.model.Parking;
import com.kt.mvc.parking.model.parkingSpace.ParkingSpaceDAO;
import com.kt.mvc.parking.model.vehicles.Car;
import com.kt.mvc.parking.model.vehicles.HistoryVehicleDAO;
import com.kt.mvc.parking.model.vehicles.VehicleDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class ParkingControllerTest {

    PrinterMVC.Controller printerController;
    ScannerMVC.Controller scannerController;
    ParkingSpaceDAO parkingSpaceDAO;
    ParkingSpaceDAO parkingSpaceDAONotMock;
    VehicleDAO vehicleDAO;
    VehicleDAO vehicleDAONotMock;
    HistoryVehicleDAO historyVehicleDAO;
    HistoryVehicleDAO historyVehicleDAONotMock;
    ParkingMVC.Model parking;
    ParkingMVC.Model parkingNotMock;
    ParkingMVC.Controller tested;
    ParkingMVC.Controller testedNotMock;

    @BeforeEach
    void setup() {
        printerController = mock(PrinterMVC.Controller.class);
        scannerController = mock(ScannerMVC.Controller.class);
        parkingSpaceDAO = mock(ParkingSpaceDAO.class);
        parkingSpaceDAONotMock = new ParkingSpaceDAO();
        vehicleDAO = mock(VehicleDAO.class);
        vehicleDAONotMock = new VehicleDAO();
        historyVehicleDAO = mock(HistoryVehicleDAO.class);
        historyVehicleDAONotMock = new HistoryVehicleDAO();
        parking = Parking.create(parkingSpaceDAO, vehicleDAO, historyVehicleDAO, printerController);
        parkingNotMock = Parking.create(parkingSpaceDAONotMock, vehicleDAONotMock, historyVehicleDAONotMock, printerController);
        tested = new ParkingController(parking, scannerController, printerController);
        testedNotMock = new ParkingController(parkingNotMock, scannerController, printerController);
        parking.reset();
    }

    @Test
    public void shouldPrintMenuWhenProgramStarted() {
        //given
        when(scannerController.pickOption()).thenReturn(12, 0);
        //when
        tested.manageParking();
        //then
        verify(printerController, times(13)).println(anyString());
    }

    @Test
    public void shouldAddCarWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(1, 0).thenReturn(12);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(vehicleDAO, times(1)).createCar(),
                () -> verify(vehicleDAO, times(1)).addVehicle(any())
        );
    }

    @Test
    public void shouldAddMotorcycleWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(2, 0).thenReturn(12);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(vehicleDAO, times(1)).createMotorcycle(),
                () -> verify(vehicleDAO, times(1)).addVehicle(any())
        );
    }

    @Test
    public void shouldAddTruckWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(3, 0).thenReturn(12);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(vehicleDAO, times(1)).createTruck(),
                () -> verify(vehicleDAO, times(1)).addVehicle(any())
        );
    }

    @Test
    public void shouldRemoveVehicleWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(4, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(1, 0);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, times(1)).println("Pojazdy w bazie"),
                () -> verify(printerController, atMostOnce()).println(parking.getVehicleDAO().getAuthorizedVehicleList()),
                () -> verify(printerController, times(1)).println("Podaj ID pojazdu"),
                () -> verify(vehicleDAO, atMostOnce()).removeVehicle(scannerController.nextInt())
        );
    }

    @Test
    public void shouldNotAllowToParkWhenVehiclenNotAuthorized() {
        //given
        when(scannerController.pickOption()).thenReturn(5, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(1, 0);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, times(1)).println("Pojazdy w bazie"),
                () -> verify(printerController, times(1)).println("Podaj ID pojazdu"),
                () -> verify(printerController, times(1)).println("Brak pozwolenia na wjazd!"),
                () -> verify(parkingSpaceDAO, times(1)).getParkingSpacesList(),
                () -> verify(vehicleDAO, times(2)).getAuthorizedVehicleList()
        );
    }

    @Test
    public void shouldAllowToParkWhenVehiclenAuthorized() {
        //given
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        when(scannerController.pickOption()).thenReturn(1, 0).thenReturn(5, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(1, 0);
        //when
        testedNotMock.manageParking();
        //then
        assertAll(
                () -> assertEquals(1, vehicleDAONotMock.getAuthorizedVehicleList().size()),
                () -> assertEquals(1, parkingSpaceDAONotMock.getParkedVehicleIdList().size()),
                () -> assertTrue(vehicleDAONotMock.getAuthorizedVehicleList()
                        .get(0).getStartTime().format(formatter).equals(LocalDateTime.now().format(formatter))),
                () -> verify(printerController, times(1)).println("Pojazdy w bazie"),
                () -> verify(printerController, times(1)).println("Podaj ID pojazdu"),
                () -> verify(printerController, times(0)).println("Brak pozwolenia na wjazd!")
        );
    }

    @Test
    public void shouldNotAllowToUnparkWhenVehiclenNotAuthorized() {
        //given
        when(scannerController.pickOption()).thenReturn(6, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(1, 0);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, times(1)).println("Zaparkowane pojazdy"),
                () -> verify(printerController, times(1)).println("Podaj ID pojazdu"),
                () -> verify(printerController, times(1)).println("Brak pojazdu w bazie!"),
                () -> verify(parkingSpaceDAO, times(1)).getParkedVehicleIdList(),
                () -> verify(vehicleDAO, times(1)).getAuthorizedVehicleList(),
                () -> verify(historyVehicleDAO, times(1)).reset()
        );
    }

    @Test
    public void shouldAllowToUnparkWhenVehiclenAuthorized() {
        //given
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        when(scannerController.pickOption()).thenReturn(1, 0).thenReturn(5, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(1, 0);
        //when
        testedNotMock.manageParking();
        //then
        assertAll(
                () -> assertEquals(1, vehicleDAONotMock.getAuthorizedVehicleList().size()),
                () -> assertEquals(1, parkingSpaceDAONotMock.getParkedVehicleIdList().size()),
                () ->  assertTrue(vehicleDAONotMock.getAuthorizedVehicleList()
                        .get(0).getStartTime().format(formatter).equals(LocalDateTime.now().format(formatter))),
                () -> verify(printerController, times(1)).println("Pojazdy w bazie"),
                () -> verify(printerController, times(1)).println("Podaj ID pojazdu"),
                () -> verify(printerController, times(0)).println("Brak pozwolenia na wjazd!")
        );
    }

    @Test
    public void shouldNotShowParkingSpacesWhenNoVehiclesParked() {
        //given
        when(scannerController.pickOption()).thenReturn(7, 0).thenReturn(12);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, atMostOnce()).println("Zaparkowane pojazdy"),
                () -> verify(parkingSpaceDAO, times(1)).getParkedVehicleIdList()
        );
    }

    @Test
    public void shouldShowParkingSpacesWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(1, 0).thenReturn(5, 0)
                .thenReturn(7, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(1, 0);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, atMostOnce()).println("Zaparkowane pojazdy"),
                () -> verify(parkingSpaceDAO, times(2)).getParkedVehicleIdList(),
                () -> verify(vehicleDAO, times(2)).getAuthorizedVehicleList()
        );
    }

    @Test
    public void shouldShowFreeParkingSpacesWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(8, 0).thenReturn(12);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, atMostOnce()).println("Wolne miejsca"),
                () -> verify(parkingSpaceDAO, times(1)).getFreeParkingSpaces()
        );
    }

    @Test
    public void shouldShowFreeParkingSpacesWhenSomeVehiclesParked() {
        //given
        when(scannerController.pickOption()).thenReturn(1, 0).thenReturn(5, 0)
                .thenReturn(8, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(1, 0);
        //when
        testedNotMock.manageParking();
        //then
        assertAll(
                () -> verify(printerController, atMostOnce()).println("Wolne miejsca"),
                () -> assertEquals(10, parkingSpaceDAONotMock.getParkingSpacesList().size()),
                () -> assertEquals(9, parkingSpaceDAONotMock.getFreeParkingSpaces().size()),
                () -> assertEquals(1, parkingSpaceDAONotMock.getParkedVehicleIdList().size())
        );
    }

    @Test
    public void shouldShowNoPaymentnsForDayWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(9, 0).thenReturn(12);
        //when
        testedNotMock.manageParking();
        //then
        assertAll(
                () -> verify(printerController, atMostOnce()).println("Dzisiejsze opłaty"),
                () -> assertEquals(0, parking.calculatePriceForDay())
        );
    }

    @Test
    public void shouldShowPaymentnsForDayWhenOptionsSelected() {
        //given
        Car car = new Car();
        car.setStartTime(LocalDateTime.now().minusMinutes(1));
        car.setEndTime(LocalDateTime.now());
        when(scannerController.pickOption()).thenReturn(9, 0).thenReturn(12);
        //when
        parkingNotMock.getHistoryVehicleDAO().addEntry(car);
        //then
        assertEquals(90, parkingNotMock.calculatePriceForDay());
    }

    @Test
    public void shouldShowEmptyHistoryWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(10, 0).thenReturn(12);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, atMostOnce()).println("Historia parkowań"),
                () -> verify(historyVehicleDAO, times(1)).getVehicleHistoryList(),
                () -> assertEquals(0, historyVehicleDAO.getVehicleHistoryList().size())
        );
    }

    @Test
    public void shouldIncreaseParkingCapacityWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(11, 0).thenReturn(12);
        when(scannerController.nextInt()).thenReturn(20, 0);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(printerController, atMostOnce()).println("Podaj nową liczbę miejsc"),
                () -> verify(parkingSpaceDAO, times(1)).increaseParkingCapacity(20)
        );
    }

    @Test
    public void shouldEndProgramWithNoInteractionsWhenOptionsSelected() {
        //given
        when(scannerController.pickOption()).thenReturn(12);
        //when
        tested.manageParking();
        //then
        assertAll(
                () -> verify(vehicleDAO, times(1)).reset(),
                () -> verify(historyVehicleDAO, times(1)).reset(),
                () -> verify(parkingSpaceDAO, times(1)).reset()
        );
    }
}